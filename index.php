<?php
   if (elgg_is_logged_in())
   {
   forward ('activity');
   }
   ?>
<!DOCTYPE html>
<html>
   <title class="w3-wide">CROWD-PYME</title>
   <!-- Mirrored from www.w3schools.com/w3css/tryw3css_templates_startup.htm by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 26 Jun 2017 22:46:00 GMT -->
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="mod/crowdpyme_theme/css/w3.css">
   <link rel="stylesheet" href="mod/crowdpyme_theme/css/w3-colors-vivid.css">
   <link rel="stylesheet" href="mod/crowdpyme_theme/css/core-login.css">
   <link rel="stylesheet" href="mod/crowdpyme_theme/css/form-login.css">
   <link rel="stylesheet" href="mod/crowdpyme_theme/css/fonts/css28b8.css?family=Raleway">
   <link rel="stylesheet" href="mod/crowdpyme_theme/css/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <style>
      body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
      body, html {
      line-height: 1.8;
      overflow: hidden;
      }
      /* Full height image header */
      .w3-bar .w3-button {
      padding: 16px;
      }
   </style>
   <body cz-shortcut-listen="true">
      <!-- Navbar (sit on top) -->
      <div class="w3-top">
         <div class="w3-bar w3-white w3-card-2" id="myNavbar">
            <i></i>
            <a href="#home" class="w3-bar-item w3-button w3-wide"> <i class="fa fa-empire w3-margin-bottom w3-center"></i>CROWD-PYME</a>
            <!-- Right-sided navbar links -->
            <div class="w3-right w3-hide-small">
            </div>
            <!-- Hide right-floated links on small screens and replace them with a menu icon -->
            <a href="javascript:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onclick="w3_open()">
            <i class="fa fa-bars"></i>
            </a>
         </div>
      </div>
      <!-- Sidebar on small screens when clicking the menu icon -->
      <nav class="w3-sidebar w3-bar-block w3-black w3-card-2 w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="mySidebar">
         <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-large w3-padding-16">Close &times;</a>
         <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button">NOSOTROS</a>
         <a href="#team" onclick="w3_close()" class="w3-bar-item w3-button">TEAM</a>
         <a href="#work" onclick="w3_close()" class="w3-bar-item w3-button">WORK</a>
         <a href="#pricing" onclick="w3_close()" class="w3-bar-item w3-button">PRICING</a>
         <a href="#contact" onclick="w3_close()" class="w3-bar-item w3-button">CONTACT</a>
      </nav>
      <!-- Header with full-height image -->
      <header class="bgimg-1 w3-display-container w3-black panel" data-section-name="container-1" id="container-1">
         <div class="w3-display-left w3-text-white" style="padding:48px">
            <span class="w3-jumbo w3-hide-small w3-teal">Bienvenido al Emprendimiento</span><br>
            <span class="w3-xxlarge w3-hide-large w3-hide-medium w3-teal">Soluciones para tu negocio y crecimiento</span><br>
            <span class="w3-large w3-teal">Acompañanos en nuestra tarea de crear un nuevas soluciones empenzando con tu interes.</span>
            <p><a href="#about" class="w3-button w3-white w3-padding-large w3-large w3-margin-top w3-opacity w3-hover-opacity-off">Comienza Ahora!</a></p>
         </div>
         <div class="w3-display-bottomright w3-text-white" style="padding:48px">
            <h1 class="w3-jumbo w3-wide w3-xxlarge w3-teal">CROWD-PYME</h1>
         </div>
         <div class="w3-display-bottomleft w3-text-grey w3-large" style="padding:24px 48px">
            <i class="fa fa-facebook-official w3-hover-opacity"></i>
            <i class="fa fa-instagram w3-hover-opacity"></i>
            <i class="fa fa-snapchat w3-hover-opacity"></i>
            <i class="fa fa-pinterest-p w3-hover-opacity"></i>
            <i class="fa fa-twitter w3-hover-opacity"></i>
            <i class="fa fa-linkedin w3-hover-opacity"></i>
         </div>
      </header>
      <!-- Introduction -->
      <div class="w3-container panel" id="container-2" data-section-name="container-2">
      <div class="w3-padding-64 w3-display-container" style="min-height: 100%;">

               <h1 class="w3-wide w3-display-left" style="font-size: 96px!important;">                  <i class="fa fa-empire w3-margin-bottom w3-center isJumbo-10rem"></i>CROWD-PYME</h1>
               <p class="w3-xlarge w3-display-right" style="margin-top: 100px;margin-right: 50px;">
                 - <i style="text-orientation: ">Red Social Orientada al Crecimiento Social y al Capital Emprendedor.</i>
               </p>
            </div>
         </div>
      </div>
      </div>  
      <div class="w3-container panel w3-vivid-red" id="container-3" data-section-name="container-3">
               <div class="w3-padding-64" style="min-height: 100%;">
                     <h2 class="w3-center w3-wide">Nuestro Valores y Objetivos</h2>
         <div class="w3-row-padding w3-center" style="margin-top:64px">
            <div class="w3-col m6">
               <button class="w3-vivid-red w3-border-0" style="outline: none;cursor: pointer;" onclick="document.getElementById('modal-mision').style.display='block'">
                  <i class="fa fa-lightbulb-o w3-margin-bottom w3-center isJumbo-10rem"></i>
                  <p class="w3-large isJumbo-2rem">MISION</p>
               </button>
            </div>
            <div class="w3-col m6 w3-border-left">
               <button class="w3-vivid-red w3-border-0" style="outline: none;cursor: pointer;" onclick="document.getElementById('modal-vision').style.display='block'">
                  <i class="fa fa-eye w3-margin-bottom isJumbo-10rem"></i>
                  <p class="w3-large isJumbo-2rem">VISION</p>
               </button>
            </div>
         </div>
         <div class="w3-row-padding w3-center" style="margin-top:64px">
           <div class="w3-panel w3-light-grey">
    <span style="font-size:150px;line-height:0.6em;opacity:0.2">❝</span>
    <p class="w3-xlarge" style="margin-top:-40px"><i><span class="w3-wide">CROWD-PYME</span> siempre estará a servicio de los usuarios que compartan un interes mutuo en el crecimiento social y en el bienestar de nuestro pais</i></p>
  </div>
         </div>
         </div>
         </div>
      </div>
      <div class="w3-container panel w3-vivid-orange" id="container-4" data-section-name="container-4">
               <div class="w3-padding-64" style="min-height: 100%;">
                     <h2 class="w3-center w3-wide">Pricipios Fundamentales de Nuestros Servicios</h2>
         <div class="w3-row-padding w3-center" style="margin-top:124px">
            <div class="w3-col s4">
               <i class="fa fa-wrench w3-margin-bottom isJumbo-10rem w3-center"></i>
               <p class="w3-xlarge w3-wide">CONSTRUIR</p>
               <p class="w3-center w3-large">
                    Elaborar un espacio que integre los principales productores y expertos en el area con el fin de producir soluciones proyectos e ideas.
               </p>
            </div>
            <div class="w3-col s4">
               <i class="fa fa-object-group w3-margin-bottom isJumbo-10rem w3-center"></i>
               <p class="w3-xlarge w3-wide">DIFUNDIR</p>
               <p class="w3-center w3-large">
                  Generar información que pueda retroalimentar beneficiosamente a la comunidad y promover la investigacion como metodo primario en nuestra plataforma.
               </p>
            </div>
            <div class="w3-col s4">
               <i class="fa fa-gears w3-margin-bottom isJumbo-10rem w3-center"></i>
               <p class="w3-xlarge w3-wide">DESARROLLAR</p>
               <p class="w3-center w3-large">
                  Desarrollar herramientas que contribuyan con la solución de las necesidades de nuestros usuarios y faciliten la depuracion y gestion de proyectos.     
               </p>
            </div>
         </div>
         </div>
         </div>
      </div>
      <div class="w3-container panel w3-vivid-greenish-blue" id="container-5" data-section-name="container-5">
               <div class="w3-padding-64" style="min-height: 100%;">
                     <h2 class="w3-center w3-wide">Nuestros Usuarios</h2>
                     <p class="w3-large w3-center">Actores Fundamentales en el Proceso de Crecimiento del Pais</p>
         <div class="w3-row-padding" style="margin-top:64px">
                  <div class="w3-row w3-hover">
                  <div class="w3-container w3-col s12 m3">
                             <div class="w3-panel w3-light-grey">
    <span style="font-size:75px;line-height:0.6em;opacity:0.2">❝</span>
    <p class="w3-large" style="margin-top:-40px"><span class="w3-wide">Contribuyen con la Producion y el Mercado Local Favoreciendo las Necedidades Locales</span></p>
  </div>
                     </div>
                     <div class="w3-container w3-col s12 m4">
                        <ul>
                           <li>
                        <p>- Principales Productores Nacionales</p>
                           </li>
                           <li>
                           <p>- 50% de las Empresas Totales del Territorio Nacional</p>
                           </li>
                           <li>
                              <p>- Comercios en Crecimiento con Mercados Felxibles</p>
                           </li>
                           <li>
                              <p>- Principales Generadores de Empleos</p>
                           </li>
                        </ul>
                     </div>
                     <div class="w3-container w3-col s12 m5">
                        <h2 class="w3-xxlarge">PYME's</h2>
                        <p class="w3-xlarge">Pequeñas y Medianas Empresas</p>
                     </div>
                     </div>
               <hr style="width:100%;border:1px solid #FFF">
                  <div class="w3-row">
                     <div class="w3-container w3-col s12 m5 w3-display-container">
                     <div class="w3-display-middel">
                                                <h2 class="w3-xxlarge">Agentes de Desarrollo</h2>
                        <p class="w3-xlarge">Personas con Iniciativa de Emprendimiento</p>
                     </div>
                     </div>
                     <div class="w3-container w3-col s12 m4">
                        <ul>
                           <li>
                        <p>- Expertos en el Area y Futuros Emprendedores</p>
                              
                           </li>
                                <li>
                        <p>- Personas Dispuestas Aprender y Compartir</p>
                                   
                                </li>
                                <li><p>- Estudiantes Universitarios, Profesionales y Agentes Locales</p></li>
                                <li>
                                   <p>- Principales Protagonistas en el eje de Contruccion de Nuevas Soluciones</p>
                                </li>     
                        </ul>
                     </div>
                     <div class="w3-container w3-col s12 m3">
                                <div class="w3-panel w3-light-grey">
    <span style="font-size:75px;line-height:0.6em;opacity:0.2">❝</span>
    <p class="w3-large" style="margin-top:-40px"><i><span class="w3-wide">Profesionales y Voluntarios Expertos en un Area Espeficica Dispuestos a Descubrir Nuevas Repuestar a las Necesidades Sociales</span></p>
  </div>
                     </div>
                  </div>
                  </div>
            </div>
         </div>
         </div>
      <div class="w3-container panel w3-vivid-red" id="container-6" data-section-name="container-6">
               <div class="w3-padding-64" style="min-height: 100%;">
                     <h2 class="w3-center w3-wide">Estado del Mercado</h2>
                     <p class="w3-large w3-center">Informacion del Mercado</p>
         <div class="w3-row-padding w3-center" style="margin-top:64px">
            <h2 class="w3-xxxlarge"><span style="font-size:75px;line-height:0.6em;opacity:0.2">❝</span>Actualmente Existe la Ausencia de un Ambiente Favorable para que PYME's del Sector Productivo Destinen Recursos ha Investigacion y Desarrollo e Proyectos:<span style="font-size:75px;line-height:0.6em;opacity:0.2">❝</span></h2>
            <div class="w3-row-padding" style="margin-top:64px">
               <div class="w3-col m3">
                  <p><span style="font-size:75px;line-height:0.6em;opacity:0.2">- 1.</span><span class="w3-large">Falta de finaciamiento de proyectos científicos y tecnológicos</span></p>
               </div>
               <div class="w3-col m3">
                  <p><span style="font-size:75px;line-height:0.6em;opacity:0.2"> - 2.</span><span class="w3-large">Falta de innovación Tecnológica</span></p>
               </div>
               <div class="w3-col m3">
                  <p><span style="font-size:75px;line-height:0.6em;opacity:0.2"> - 3.</span><span class="w3-large">Ausencia de políticas de impulso industrial</span></p>
               </div>
               <div class="w3-col m3">
                  <p><span style="font-size:75px;line-height:0.6em;opacity:0.2"> - 4.</span><span class="w3-large">Poca disposición hacia la asociatividad</span></p>
               </div>
            </div>
         </div>
         </div>
      </div>
      <div class="w3-container panel w3-vivid-green" id="container-7" data-section-name="container-7">
               <div class="w3-padding-64" style="min-height: 100%;">
                     <h2 class="w3-center w3-wide">Nuestros Productos</h2>
                     <p class="w3-large w3-center">Ofrecemos a Nuestos Usuarios Herramientas que Fortalezcan su Proceso de Creciminto</p>
         <div class="w3-row-padding" style="margin-top:42px">
            <div class="w3-row">
               <div class="w3-quarter w3-center">
                  <i class="fa fa-connectdevelop w3-margin-bottom isJumbo-10rem"></i>
                  <h4>Servicio de Red Social</h4>
               </div>
               <div class="w3-quarter w3-center">
                  <i class="fa fa-code-fork w3-margin-bottom isJumbo-10rem"></i>
                  <h4>Repositorio Digital de Proyectos</h4>
               </div>
               <div class="w3-quarter w3-center">
                  <i class="fa fa-edit w3-margin-bottom isJumbo-10rem"></i>
                  <h4>Gestion de Contenido</h4>
               </div>
               <div class="w3-quarter w3-center">
                  <i class="fa fa-first-order w3-margin-bottom isJumbo-10rem"></i>
                  <h4>Campañas de Investigacion y Finaciamiento</h4>
               </div>
            </div>
            <div class="w3-row">
               <div class="w3-quarter w3-center">
                  <i class="fa fa-object-ungroup w3-margin-bottom isJumbo-10rem"></i>
                  <h4>Gestion y Generacion de Proyectos</h4>
               </div>
               <div class="w3-quarter w3-center">
                  <i class="fa fa-suitcase w3-margin-bottom isJumbo-10rem"></i>
                  <h4>Portafolios de Servicios</h4>
               </div>
               <div class="w3-quarter w3-center">
                  <i class="fa fa-info w3-margin-bottom isJumbo-10rem"></i>
                  <h4>Administracion de Informacion</h4>
               </div>
               <div class="w3-quarter w3-center">
                  <i class="fa fa-support w3-margin-bottom isJumbo-10rem"></i>
                  <h4>Seguridad y Soporte</h4>
               </div>
            </div>
         </div>
         </div>
      </div>

      <div class="w3-container panel w3-vivid-reddish-orange" id="container-8" data-section-name="container-8">
               <div class="w3-padding-64" style="min-height: 100%;">
<h3 class="w3-center">Equipo <span class="w3-wide">CROWD-PYME</span></h3>
  <p class="w3-center w3-large">Actualmente en Nuestro Catalogo de Emprendedores</p>
  <div class="w3-row-padding w3-grayscale w3-center" style="margin-top:64px">
    <div class="w3-col l6 m6 w3-margin-bottom">
      <div class="w3-card-2 w3-vivid-white" >
        <div class="w3-container">
          <h3>Gisel Marie</h3>
          <p class="w3-opacity">Fundadora y Participante</p>
          <p>"Esfuerzo y Amor"</p>
          <p><button class="w3-button w3-light-grey w3-block"><i class="fa fa-envelope"></i> Contactar</button></p>
        </div>
      </div>
    </div>
    <div class="w3-col l6 m6 w3-margin-bottom">
      <div class="w3-card-2 w3-vivid-white">
        <div class="w3-container">
          <h3>Hector Acuña</h3>
          <p class="w3-opacity">Fundador y Participante</p>
          <p>"Avanzar y Transformar al Mundo"</p>
          <p><button class="w3-button w3-light-grey w3-block"><i class="fa fa-envelope"></i> Contactar</button></p>
        </div>
      </div>
    </div>

         </div>
         </div>
      </div>

            <div class="w3-container panel" id="container-9" data-section-name="container-9">
               <div class="w3-padding-64" style="min-height: 100%;">
                     <h2 class="w3-center w3-wide">Dentro del Motor de Emprendimiento</h2>
                     <p class="w3-large w3-center">Herramientas con las Cuales fue Realizado <span class="w3-wide">CROWD-PYME</span></p>
         <div class="w3-row-padding w3-center" style="margin-top:146px">
               <div class="w3-third">
                  <h4 class="w3-xlarge">L.A.M.P</h4>
                  <p class="w3-large">Linux - Apache - MySql - PHP</p>
                  <div class="w3-panel">
                     <p>Motor de el Servicio de Ejecucion de Nuestra Plataforma</p>
                  </div>
               </div>
               <div class="w3-third">
                  <h4 class="w3-xlarge">elgg</h4>
                  <p class="w3-large">Social Scripting</p>
                  <div class="w3-panel">
                     <p>Motor de Red Social GNU - Libre y Poderoso</p>
                  </div>
               </div>
               <div class="w3-third">
                  <h4 class="w3-xlarge">ndt</h4>
                  <p class="w3-large">Metodologia de Desarrollo</p>
                  <div class="w3-panel">
                     <p>Herramienta Esencial para la Construcion de Plataformas Orientada a Usuarios</p>
                  </div>
               </div>
         </div>
         </div>
      </div>
      </div>
      <!-- Modal Section-->

      <!-- MODAL LOGIN / REGISTER -->
      <div id="id01" class="w3-modal">
            <div class="w3-container">
               <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
               <div class="module form-module">
                  <div class="toggle">
                     <i class="fa fa-times fa-pencil"></i>
                  </div>
                  <div class="form">
                     <h2>Acceso a <span class="w3-wide">CROWD-PYME</span></h2>
                     <form action="action/login" method="post">
                        <?php 
                        $ts = time(); 
                        $token = generate_action_token($ts); 
                        ?>
                        <input type="text" name="username" placeholder="Nombre de Usuario"/>
                        <input type="password" name="password" placeholder="Contraseña"/>
                        <input type="hidden" name="__elgg_token" value="<?php echo $token; ?>"/>
                        <input type="hidden" name="__elgg_ts" value="<?php echo $ts; ?>" />
                        <input type="submit" name="submit" value="Acceder">
                     </form>
                  </div>
                  <div class="form">
                     <h2>Creacion de Cuenta</h2>
                     <form action="action/register" method="post">
                        <input type="text" placeholder="Nombre de Usuario"/>
                        <input type="email" placeholder="Correo Electronico"/>
                        <input type="tel" placeholder="Numero de Telefono"/>
                        <input type="password" placeholder="Contraseña"/>
                        <input type="password" placeholder="Repetir Contraseña"/>
                        <button>Registrar</button>
                     </form>
                  </div>
                  <div class="cta"><a href="http://andytran.me">Recuperar Contraseña</a></div>
               </div>
            </div>
      </div>
      <!-- MODAL MISION -->
      <div id="modal-mision" class="w3-modal w3-white" onclick="this.style.display='none'" style="padding: 0px !important">
         <div class="w3-animate-left container-flex">
            <span class="w3-button w3-large w3-vivid-red w3-display-topleft" title="Close Modal Image" style="z-index: 1;    width: 25.35%;"><i class="fa fa-remove"></i></span>
            <div class="w3-container container-flex" style="padding: 0px !important">
               <div class="isIcon w3-vivid-red w3-display-container">
                  <div class="w3-display-middle">
                     <i class="fa fa-lightbulb-o w3-margin-bottom isJumbo-10rem w3-center"></i>
                  </div>
               </div>
               <div class="isContent" style="margin:0px 50px 0px 50px">
                  <h1 class="w3-xxxlarge w3-wide w3-right-align"><b style="color: #be0032">MISION</b></h1>
                  <hr style="border:5px solid #be0032;" class="w3-round">
                  <div class="w3-center w3-xlarge">
                     <p>"La misión de Crowd-PyMes es crear un ambiente favorable por medio de la integración, herramientasroles, y acuedos de servicio, para que PyMes del sector productivo Venezolano y agentes de desarrollodestinen recursos a investigación y desarrollo de proyectos impulsando las innovaciones tecnologicasy descubriendo nuevas respuestas a las necesidades sociales, de igual forma se busca motivar
                     a las universidades a realizar campañas de investigación y concursos de proyectos exponiendo los valores
                     del producto de una idea que aun esta en desarrollo "
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      </div>
      <!-- MODAL VISION -->
      <div id="modal-vision" class="w3-modal w3-white" onclick="this.style.display='none'" style="padding: 0px !important">
         <div class="w3-animate-right container-flex">
            <span class="w3-button w3-large w3-vivid-red w3-display-topright" title="Close Modal Image" style="z-index: 1;    width: 25.35%;"><i class="fa fa-remove"></i></span>
            <div class="w3-container container-flex" style="padding: 0px !important">
               <div class="isContent" style="margin:0px 50px 0px 50px">
                  <h1 class="w3-xxxlarge w3-wide w3-left-align"><b style="color: #be0032">VISION</b></h1>
                  <hr style="border:5px solid #be0032;" class="w3-round">
                  <div class="w3-center w3-xlarge">
                     <p>
                        "Ser un nuevo modelo de mercado que contribuya con el crecimiento social y tecnológico del pais.
                         Buscamos transformarnos en un recurso ṕara los profecionales que estan comenzando sus camino
                         de emprendimiento"
                     
                     </p>
                  </div>
               </div>
               <div class="isIcon w3-vivid-red w3-display-container">
                  <div class="w3-display-middle">
                     <i class="fa fa-eye w3-margin-bottom isJumbo-10rem w3-center"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Footer Info -->
      <div class="w3-container panel w3-vivid-red" data-section-name="container-10" id="container-10">
      <div class="w3-row-padding w3-center">
         <div class="w3-row-padding w3-center" style="margin-top:64px">
            <div class="w3-half hover-foo-info">
               <h2>Contactanos</h2>
               <div class="w3-left">
                  <p style="text-align: left;"><i class="fa fa-map-marker fa-fw w3-xxlarge w3-margin-right"></i> Valencia, Venezuela</p>
                  <p style="text-align: left;"><i class="fa fa-phone fa-fw w3-xxlarge w3-margin-right"></i> Telefono: +58414191676</p>
                  <p style="text-align: left;"><i class="fa fa-envelope fa-fw w3-xxlarge w3-margin-right"> </i> Email: crowdpyme@gmail.com</p>
               </div>
               <br>
               <form action="https://www.w3schools.com/action_page.php" target="_blank">
                  <p><input class="w3-input w3-border" type="text" placeholder="Nombre" required name="Name"></p>
                  <p><input class="w3-input w3-border" type="text" placeholder="Correo Electronico" required name="Email"></p>
                  <p><input class="w3-input w3-border" type="text" placeholder="Asunto" required name="Subject"></p>
                  <p><input class="w3-input w3-border" type="text" placeholder="Mensaje" required name="Message"></p>
                  <p>
                     <button class="w3-button w3-black" type="submit">
                     <i class="fa fa-paper-plane"></i> ENVIAR
                     </button>
                  </p>
               </form>
            </div>
            <div class="w3-half">
               <h2>Informacion Adicional</h2>
               <div class="w3-row">
                  <div class="w3-col m4 s12 w3-border-left">
                     <ul class="w3-ul no-border">
                        <h4>Proyectos</h4>
                        <li class="w3-small"><a href="">
                           Orgamon - Plataforma de Gestion Municipal
                           </a>
                        </li>
                        <li class="w3-small"><a href="">
                           Impress - CMS [Estudiantes - Profesores]
                           </a>
                        </li>
                        <li class="w3-small"><a href="">
                           Moravo - Red Social de Pensamiento Divergente
                           </a>
                        </li>
                        <li class="w3-small"><a href=""></a></li>
                     </ul>
                  </div>
                  <div class="w3-col m4 s12 w3-border-left">
                     <ul class="w3-ul no-border">
                        <h4>Actividades</h4>
                        <li class="w3-small"><a href="">Seminario de Crowd-PyME</a></li>
                        <li class="w3-small"><a href="">Reunion de PyMEs - Occidente</a></li>
                        <li class="w3-small"><a href=""></a></li>
                        <li class="w3-small"><a href=""></a></li>
                     </ul>
                  </div>
                  <div class="w3-col m4 s12 w3-border-left">
                     <ul class="w3-ul no-border">
                        <h4>Campañas</h4>
                        <li class="w3-small"><a href="">Campaña de Investigacion</a></li>
                        <li class="w3-small"><a href="">Campaña de Cultura de Emprendimiento</a></li>
                        <li class="w3-small"><a href="">El Guion del Crecimiento</a></li>
                        <li class="w3-small"><a href="">Formacion de Profesionales</a></li>
                     </ul>
                  </div>
               </div>
               <div class="w3-row-padding" style="margin-top: 130px;">
                  <div class="w3-container">
                     <footer class="w3-center">
                        <a class="w3-button w3-light-grey scroll-top"><i class="fa fa-arrow-up w3-margin-right"></i>Al Inicio</a>
                        <a class="w3-button w3-light-grey" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-arrow-up w3-margin-right"></i>Acceder</a>
                        <div class="w3-xlarge w3-section">
                           <i class="fa fa-facebook-official w3-hover-opacity"></i>
                           <i class="fa fa-instagram w3-hover-opacity"></i>
                           <i class="fa fa-snapchat w3-hover-opacity"></i>
                           <i class="fa fa-pinterest-p w3-hover-opacity"></i>
                           <i class="fa fa-twitter w3-hover-opacity"></i>
                           <i class="fa fa-linkedin w3-hover-opacity"></i>
                        </div>
                        <p class="w3-">Powered by <a href="default.html" title="W3.CSS" target="_blank" class="w3-hover-text-white w3-wide">Crowd-PyME Team / w3.css</a></p>
                     </footer>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Fixed Navigation -->
      <ul class="pagination">
         <li><a href="#container-1" class="active"></a></li>
         <li><a href="#container-2" class=""></a></li>
         <li><a href="#container-3" class=""></a></li>
         <li><a href="#container-4" class=""></a></li>
         <li><a href="#container-5" class=""></a></li>
         <li><a href="#container-6" class=""></a></li>
         <li><a href="#container-7" class=""></a></li>
         <li><a href="#container-8" class=""></a></li>
         <li><a href="#container-9" class=""></a></li>
         <li><a href="#container-10" class=""></a></li>

      <!-- Add js.srollifity -->
      <script src="mod/crowdpyme_theme/js/libs/jquery-2.1.1.js"></script>
      <script src="mod/crowdpyme_theme/js/jquery.scrollify.js"></script>
      <script src="mod/crowdpyme_theme/js/core-index.js"></script>
      <script>
         // Scroll to Top
         $('.scroll-top').click(function(){
         $("html, body").animate({ scrollTop: 0 }, 600);
         return false;
         });
         //
         
            // Slide Gallery
            var slideIndex = 1;
            showDivs(slideIndex);
            
            function plusDivs(n) {
              showDivs(slideIndex += n);
            }
            
            function currentDiv(n) {
              showDivs(slideIndex = n);
            }
            
            function showDivs(n) {
              var i;
              var x = document.getElementsByClassName("li-content");
              var dots = document.getElementsByClassName("li-selector");
              if (n > x.length) {slideIndex = 1}    
              if (n < 1) {slideIndex = x.length}
              for (i = 0; i < x.length; i++) {
                 x[i].style.display = "none";  
              }
              for (i = 0; i < dots.length; i++) {
                 dots[i].className = dots[i].className.replace(" w3-red", "");
              }
              x[slideIndex-1].style.display = "block";  
              dots[slideIndex-1].className += " w3-red";
            }
            
            // Toggle between showing and hiding the sidebar when clicking the menu icon
            var mySidebar = document.getElementById("mySidebar");
            
            function w3_open() {
                if (mySidebar.style.display === 'block') {
                    mySidebar.style.display = 'none';
                } else {
                    mySidebar.style.display = 'block';
                }
            }
            
            // Close the sidebar with the close button
            function w3_close() {
                mySidebar.style.display = "none";
            }
      </script>
   </body>
</html>