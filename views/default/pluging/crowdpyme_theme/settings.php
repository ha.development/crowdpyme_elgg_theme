<?php

/**
 */
$image_path = $vars['entity']->image_path;
$secondary_theme = $vars['entity']->secondary_theme;
$navbar_style = $vars['entity']->navbar_style;
$panel_type = $vars['entity']->panel_type;

echo elgg_view_field(array(
    "#label" => "Logo Path",
    "#type" => "text",
    "value" => $image_path,
    "name" => "params[image_path]"
));

echo elgg_view_field(array(
    "#label" => "Navbar Style",
    "#type" => "dropdown",
    "value" => $navbar_style,
    "name" => "params[navbar_style]",
    "options_values" => array(
        "default" => "Default",
        "inverse" => "Inverse"
    )
));

echo elgg_view_field(array(
    "#label" => "Panel Type",
    "#type" => "dropdown",
    "value" => $panel_type,
    "name" => "params[panel_type]",
    "options_values" => array(
        "panel-default" => elgg_echo("panel-default"),
        "panel-primary" => elgg_echo("panel-primary"),
        "panel-success" => elgg_echo("panel-success"),
        "panel-info" => elgg_echo("panel-info"),
        "panel-warning" => elgg_echo("panel-warning"),
        "panel-danger" => elgg_echo("panel-danger")
    )
));

echo elgg_view_field(array(
    "#label" => "Secondary Theme",
    "#type" => "dropdown",
    "value" => $secondary_theme,
    "name" => "params[secondary_theme]",
    "options_values" => socia_available_themes()
));
