<?php
/**
 * Elgg topbar wrapper
 * Check if the user is logged in and display a topbar
 * @since 1.10 
 */
?>
<div class="w3-top">
  <div class="w3-bar w3-vivid-blue" style="overflow: inherit !important;">
		<?php
		echo elgg_view('page/elements/topbar', $vars);    
		?>
</div>