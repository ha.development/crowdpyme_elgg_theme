<?php

/**
 * Twitter&reg; Bootstrap Theme for Elgg
 *
 * Converts all Elgg css elements to Twitter&reg; Bootstrap elements.  Helps 
 * Designers create beautiful Bootstrap themes for Elgg.
 *
 * PHP version 5.6
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   Elgg Themes
 * @author     Shane Barron <admin@socia.us>
 * @copyright  2017 SocialApparatus
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1
 * @link       http://socia.us
 */
$messages = elgg_view('page/elements/messages', array('object' => $vars['sysmessages']));

$header = elgg_view('page/elements/header', $vars);
$navbar = elgg_view('page/elements/navbar', $vars);
$content = elgg_view('page/elements/body', $vars);
$footer = elgg_view('page/elements/footer', $vars);

$body = <<<__BODY
<div class="elgg-page elgg-page-default">
	<div class="elgg-page-messages">
		$messages
	</div>
__BODY;

$body .= elgg_view('page/elements/topbar_wrapper', $vars);

$body .= <<<__BODY
<div>
    $header
</div>
<nav class="w3-sidebar w3-bar-block w3-small w3-hide-small w3-center" id="main-sidebar">
    $navbar
</div>
<div id="main-container">
    $content
    <div class="w3-bottom">
        <div class="w3-padding-32 w3-gray w3-display-container">
        <div class="w3-display-right" style="margin-right:135px;">
                <h4 class="w3-wide">CROWD-PYME TEAM</h4>
            </div>
            <div class="w3-display-left w3-margin-left cl-effect-1">
                $footer
            </div>
        </div>
    </div>
</div>
            

__BODY;

$body .= elgg_view('page/elements/foot');

$head = elgg_view('page/elements/head', $vars['head']);

$params = array(
    'head' => $head,
    'body' => $body,
);

if (isset($vars['body_attrs'])) {
    $params['body_attrs'] = $vars['body_attrs'];
}

echo elgg_view("page/elements/html", $params);
