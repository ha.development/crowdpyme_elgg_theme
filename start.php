<?php

elgg_register_event_handler('init', 'system', 'crowdpyme_theme_init');

function crowdpyme_theme_init() {

    elgg_extend_view("elgg.css", "crowdpyme_theme/css");
    
    elgg_register_event_handler('pagesetup', 'system', 'crowdpyme_theme_pagesetup', 1000);

    // registered with priority < 500 so other plugins can remove likes
    elgg_unregister_plugin_hook_handler('register', 'menu:river', 'likes_river_menu_setup');
    elgg_unregister_plugin_hook_handler('register', 'menu:entity', 'likes_entity_menu_setup');
    elgg_unregister_plugin_hook_handler('register', 'menu:river', '_elgg_river_menu_setup');
    elgg_unregister_plugin_hook_handler('register', 'menu:entity', '_elgg_entity_menu_setup');
    elgg_unregister_plugin_hook_handler('register', 'menu:topbar', 'messages_register_topbar');

    elgg_register_plugin_hook_handler('register', 'menu:river', '_my_elgg_river_menu_setup');
    elgg_register_plugin_hook_handler('register', 'menu:entity', '_my_elgg_entity_menu_setup');
    elgg_register_plugin_hook_handler('register', 'menu:river', 'my_likes_river_menu_setup', 401);
    elgg_register_plugin_hook_handler('register', 'menu:entity', 'my_likes_entity_menu_setup', 401);

    elgg_unextend_view('page/elements/header', 'search/header');
    elgg_extend_view('page/elements/sidebar', 'search/header', 0);

    //Menus
    elgg_register_plugin_hook_handler("register", "menu:site", "elggbook_iconized_menu");
    elgg_unregister_plugin_hook_handler('prepare', 'menu:site', '_elgg_site_menu_setup');

    if (!elgg_in_context("admin")) 
    {
    // Load css/js
    $css_path = elgg_get_site_url() . "mod/crowdpyme_theme/assets/";
    $plugin_path = elgg_get_site_url() ."mod/crowdpyme_theme/";

    elgg_register_css("w3css", $css_path . "css/w3.css");
    elgg_register_css("w3colorsvivid", $css_path . "css/w3-colors-vivid.css");
    elgg_register_css("component", $plugin_path . "css/component.css");

    //elgg_register_css("search-engine", $plugin_path . "css/search-engine.css");
    //elgg_register_js("search-engine", $plugin_path . "js/search-engine.js");

    //Load CSS
    elgg_load_css("w3css");
    elgg_load_css("w3colorsvivid");
    elgg_load_css("component");
    //elgg_load_css("search-engine");

    //Load js
    //elgg_load_js("search-engine");
    
    }
    else
    {
        //CSS
        elgg_unregister_css("w3css");
        elgg_unregister_css("w3colorsvivid");
        elgg_unregister_css("component");
        //elgg_unregister_css("search-engine");   

        //JS
        //elgg_unregister_js("search-engine");

    }    
    elgg_register_plugin_hook_handler('head', 'page', 'crowdpyme_theme_setup_head');

    // non-members do not get visible links to RSS feeds
    if (!elgg_is_logged_in()) {
        elgg_unregister_plugin_hook_handler('output:before', 'layout', 'elgg_views_add_rss_link');
    }

    elgg_register_plugin_hook_handler('index','system','crowdpyme_theme_index');

}
/**
 * Serve the index page
 * 
 * @return bool Whether the page was sent.
 */
function crowdpyme_theme_index() {
    if (!include_once(dirname(__FILE__) . "/index.php")) {
        return false;
    }

    return true;
}

/**
 * Register items for the html head
 *
 * @param string $hook Hook name ('head')
 * @param string $type Hook type ('page')
 * @param array  $data Array of items for head
 * @return array
 */
function crowdpyme_theme_setup_head($hook, $type, $data) {
    $data['metas'][] = array(
        'name' => 'viewport',
        'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0',
    );
    // https://developer.chrome.com/multidevice/android/installtohomescreen
    $data['metas'][] = array(
        'name' => 'mobile-web-app-capable',
        'content' => 'yes',
    );

    $data['metas'][] = array(
        'name' => 'apple-mobile-web-app-capable',
        'content' => 'yes',
    );
    return $data;
}

function elggbook_iconized_menu($hook, $type, $items, $params) {

    $name_to_icon = array(
        "activity" => '<i class="fa  fa-globe fa-lg"></i>',
        "bookmarks" => '<i class="fa  fa-link fa-lg"></i>',
        "thewire" => '<i class="fa  fa-pencil-square fa-lg"></i>',
        "groups" => '<i class="fa  fa-circle-o fa-lg"></i>',
        "file" => '<i class="fa  fa-paperclip fa-lg"></i>',
        "pages" => '<i class="fa  fa-list-alt fa-lg"></i>',
        "members" => '<i class="fa  fa-user fa-lg"></i>',
        "blog" => '<i class="fa  fa-bold fa-lg"></i>',
        "videolist" => '<i class="fa  fa-video-camera fa-lg"></i>',
        "event_manager" => '<i class="fa  fa-calendar-o fa-lg"></i>',
        'event_calendar' => '<i class="fa  fa-calendar-o fa-lg"></i>',
        "poll" => '<i class="fa  fa-check-square fa-lg"></i>',
        "photos" => '<i class="fa  fa-camera fa-lg"></i>',
        "FAQ" => '<i class="fa  fa-question-circle-o fa-lg"></i>',
        "scheduling" => '<i class="fa  fa-calendar-check-o fa-lg"></i>',
        "etherpad" => '<i class="fa  fa-wpforms fa-lg"></i>',
        "discussion" => '<i class="fa  fa-stack-exchange fa-lg"></i>',
        "translation_editor" => '<i class="fa  fa-language fa-lg"></i>',
        "dashboard" => '<i class="fa  fa-dashboard fa-lg"></i>',
        "groups-add" => '<i class="fa  fa-circle-o-notch fa-lg"></i>',
        "groups:member" => '<i class="fa  fa-circle fa-lg"></i>',
        'izap_videos' => '<i class="fa  fa-film fa-lg"></i>',
        'videos' => '<i class="fa  fa-film fa-lg"></i>',
        'video' => '<i class="fa  fa-film fa-lg"></i>',
        'videolist' => '<i class="fa  fa-play fa-lg"></i>',
        'photo_albums' => '<i class="fa  fa-image fa-lg"></i>',
        'gallery' => '<i class="fa  fa-camera fa-lg"></i>',
        'wall' => '<i class="fa  fa-building fa-lg"></i>',
        'group:albums' => '<i class="fa  fa-image fa-lg"></i>',
        'user:albums' => '<i class="fa  fa-image fa-lg"></i>',
        'images' => '<i class="fa  fa-image fa-lg"></i>',
        'market' => '<i class="fa  fa-shopping-cart fa-lg"></i>',
        'events' => '<i class="fa  fa-calendar fa-lg"></i>',
    );
    $icon_name = "pippo"; //set a fake default
    foreach ($items as $key => $item) {

        $icon_name = $name_to_icon[$item->getName()];
        $item->setText(elgg_view_icon($icon_name) . $item->getText());
    }

    return $items;
}
/**
 * Rearrange menu items
 */
function crowdpyme_theme_pagesetup() {

    elgg_unextend_view('page/elements/sidebar', 'search/header');
    //elgg_extend_view('page/elements/topbar', 'search/header', 0);

    elgg_register_menu_item('topbar', array(
        'name' => 'sidebar',
        'id' => 'open-slidebar',
        'href' => "#",
        'text' => '<i class="sb-toggle-left fa fa-bars fa-lg"></i>',
        'priority' => 50,
        'link_class' => '',
    ));

    elgg_unregister_menu_item('footer','powered');
        
    if (elgg_is_logged_in()) {
        $user = elgg_get_logged_in_user_entity();
        $username = $user->username;        
        
        elgg_unregister_menu_item('menu:topbar','messages');
        //elgg_register_plugin_hook_handler('register', 'menu:topbar', 'messages_register_topbar');
        $text = "<i class=\"fa fa-envelope fa-lg\"></i>";
        $tooltip = elgg_echo("messages");
        // get unread messages
        $num_messages = (int)messages_count_unread();
        if ($num_messages != 0) {
            $text .= "<span class=\"elgg-topbar-new\">$num_messages</span>";
            $tooltip .= ": ".elgg_echo("messages:unreadcount", array($num_messages));
        }

        elgg_register_menu_item('topbar', array(
            'name' => 'messages',
            'href' => "messages/inbox/$username",
            'text' => $text,
            'section' => 'alt',
            'priority' => 100,
            'title' => $tooltip,
        ));
        
        elgg_register_menu_item('topbar', array(
            'href' => false,
            'name' => 'search',         
            'text' => '<i class="fa fa-search fa-lg"></i>'.elgg_view('search/header'),          
            'priority' => 0,
            'section' => 'alt',
        ));

        $text = '<i class="fa fa-users fa-lg"></i>';
        $tooltip = elgg_echo("friends");
        $href = "/friends/".$username;
        if (elgg_is_active_plugin('friend_request')) {
            elgg_unregister_menu_item('topbar', 'friend_request');
            $options = array(
                "type" => "user",
                "count" => true,
                "relationship" => "friendrequest",
                "relationship_guid" => $user->getGUID(),
                "inverse_relationship" => true
            );
            
            $count = elgg_get_entities_from_relationship($options);
            if (!empty($count)) {
                $text .= "<span class=\"elgg-topbar-new\">$count</span>";
                $tooltip = elgg_echo("friend_request:menu").": ".$count;
                $href = "friend_request/" . $username;
            }
        }

        elgg_unregister_menu_item('topbar', 'friends');
        elgg_register_menu_item('topbar', array(
            'href' => $href,
            'name' => 'friends',
            'text' =>  $text,
            'section' => 'alt',
            'priority' => 200,
            'title' => $tooltip,
        ));

        $viewer = elgg_get_logged_in_user_entity();
        elgg_unregister_menu_item('topbar', 'profile');
        elgg_register_menu_item('topbar', array(
            'name' => 'profile',
            'href' => $viewer->getURL(),
            'title' => $viewer->name,
            'text' => elgg_view('output/img', array(
                'src' => $viewer->getIconURL('small'),
                'alt' => $viewer->name,
                'title' => $viewer->name,
                'class' => 'elgg-border-plain elgg-transition',
            )).'<span class="profile-text">'.elgg_get_excerpt($viewer->name, 20).'</span>',
            'priority' => 500,
            'link_class' => 'elgg-topbar-avatar',
            'item_class' => 'elgg-avatar elgg-avatar-topbar',
        ));

        elgg_register_menu_item('topbar', array(
            'name' => 'home',
            'text' => '<i class="fa fa-home fa-lg"></i> ',
            'href' => "/",
            'priority' => 90,
            'section' => 'alt',
        ));

        elgg_register_menu_item('topbar', array(
            'name' => 'account',
            'text' => '<i class="fa fa-cog fa-lg"></i> ',
            'href' => "#",
            'priority' => 300,
            'section' => 'alt',
            'link_class' => 'elgg-topbar-dropdown',
        ));

        if (elgg_is_active_plugin('dashboard')) {
            $item = elgg_unregister_menu_item('topbar', 'dashboard');
            /*
            if ($item) {
                $item->setText(elgg_echo('dashboard'));
                $item->setSection('default');
                elgg_register_menu_item('site', $item);
            }*/
        }

        $item = elgg_unregister_menu_item('extras', 'bookmark');
        if ($item) {
            $item->setText('<i class="fa fa-bookmark fa-lg"></i>');
            elgg_register_menu_item('extras', $item);
        }

      elgg_unregister_menu_item('extras', 'rss');
      
        $url = elgg_format_url($url);
        elgg_register_menu_item('extras', array(
            'name' => 'rss',
            'text' => '<i class="fa fa-rss fa-lg"></i>',
            'href' => $url,
            'title' => elgg_echo('feed:rss'),
        ));
        
        $item = elgg_get_menu_item('topbar', 'usersettings');
        if ($item) {
            $item->setParentName('account');
            $item->setText(elgg_echo('settings'));
            $item->setPriority(103);
        }

        $item = elgg_get_menu_item('topbar', 'logout');
        if ($item) {
            $item->setParentName('account');
            $item->setText(elgg_echo('logout'));
            $item->setPriority(104);
        }

        $item = elgg_get_menu_item('topbar', 'administration');
        if ($item) {
            $item->setParentName('account');
            $item->setText(elgg_echo('admin'));
            $item->setPriority(101);
        }

        if (elgg_is_active_plugin('site_notifications')) {
            $item = elgg_get_menu_item('topbar', 'site_notifications');
            if ($item) {
                $item->setParentName('account');
                $item->setText(elgg_echo('site_notifications:topbar'));
                $item->setPriority(102);
            }
        }

        if (elgg_is_active_plugin('reportedcontent')) {
            $item = elgg_unregister_menu_item('footer', 'report_this');
            if ($item) {
                $item->setText('<i class="fa fa-flag fa-lg"></i>');
                $item->setPriority(500);
                $item->setSection('default');
                elgg_register_menu_item('extras', $item);
            }
        }

        elgg_register_menu_item('page', array(
            'name' => 'edit_cover',
            'href' => "cover/edit/{$username}",
            'text' => elgg_echo('cover:edit'),
            'section' => '1_profile',
            'contexts' => array('settings'),
        ));
        
    }
    else{
        if (elgg_get_context()=='main')
            $href= "#login";
        else
            $href= "/#login";

            elgg_register_menu_item('topbar', array(
                'name' => 'login',
                'text' => elgg_echo('login'),
                'href' => $href,
                'priority' => 90,
                'section' => 'alt',
                'class' => 'btn btn-info elgg-button elgg-button-submit go-login'
            ));
    }
}
function my_likes_entity_menu_setup($hook, $type, $return, $params) {
    if (elgg_in_context('widgets')) {
        return $return;
    }

    $entity = $params['entity'];
    /* @var ElggEntity $entity */

    if ($entity->canAnnotate(0, 'likes')) {
        $hasLiked = \Elgg\Likes\DataService::instance()->currentUserLikesEntity($entity->guid);
        
        // Always register both. That makes it super easy to toggle with javascript
        $return[] = ElggMenuItem::factory(array(
            'name' => 'likes',
            'href' => elgg_add_action_tokens_to_url("/action/likes/add?guid={$entity->guid}"),
            'text' => '<i class="fa fa-thumbs-o-up fa-lg"></i>',
            'title' => elgg_echo('likes:likethis'),
            'item_class' => $hasLiked ? 'hidden' : '',
            'priority' => 100,
        ));
        
        $return[] = ElggMenuItem::factory(array(
            'name' => 'unlike',
            'href' => elgg_add_action_tokens_to_url("/action/likes/delete?guid={$entity->guid}"),
            'text' => '<i class="fa  fa-thumbs-up fa-lg" style="color:#50C28C"></i>',
            'title' => elgg_echo('likes:remove'),
            'item_class' => $hasLiked ? '' : 'hidden',
            'priority' => 100,
        ));
    }
    
    // likes count
    $count = elgg_view('likes/count', array('entity' => $entity));
    if ($count) {
        $options = array(
            'name' => 'likes_count',
            'text' => $count,
            'href' => false,
            'priority' => 101,
        );
        $return[] = ElggMenuItem::factory($options);
    }

    return $return;
}
function _my_elgg_entity_menu_setup($hook, $type, $return, $params) {
    if (elgg_in_context('widgets')) {
        return $return;
    }
    
    $entity = $params['entity'];
    /* @var \ElggEntity $entity */
    $handler = elgg_extract('handler', $params, false);

    // access
    if (elgg_is_logged_in()) {
        $access = elgg_view('output/access', array('entity' => $entity));
        $options = array(
            'name' => 'access',
            'text' => $access,
            'href' => false,
            'priority' => 100,
        );
        $return[] = \ElggMenuItem::factory($options);
    }
    
    if ($entity->canEdit() && $handler) {
        // edit link
        $options = array(
            'name' => 'edit',
            'text' => '<i class="fa fa-edit fa-lg"></i>',
            'title' => elgg_echo('edit:this'),
            'href' => "$handler/edit/{$entity->getGUID()}",
            'priority' => 200,
        );
        $return[] = \ElggMenuItem::factory($options);

        // delete link
        $options = array(
            'name' => 'delete',
            'text' => '<i class="fa fa-trash-o fa-lg"></i>',
            'title' => elgg_echo('delete:this'),
            'href' => "action/$handler/delete?guid={$entity->getGUID()}",
            'confirm' => elgg_echo('deleteconfirm'),
            'priority' => 300,
        );
        $return[] = \ElggMenuItem::factory($options);
    }

    return $return;
}
